// source: https://stackoverflow.com/questions/14446511/most-efficient-method-to-groupby-on-an-array-of-objects
export const groupBy = (list: Array<any>, key: string) => {
  return list.reduce((a, b) => {
    (a[b[key]] = a[b[key]] || []).push(b);
    return a;
  }, {});
};
