export interface IStockTransaction {
  name: number;
  description: string;
  createdAt: Date;
  status: string;
}
