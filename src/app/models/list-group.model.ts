import { IStockTransaction } from './stock-transaction.model';

export enum EListGroup {
  createdAt = 'createdAt',
  status = 'status'
}

export interface IGroupedByStatus {
  pending?: IStockTransaction[];
  completed?: IStockTransaction[];
  rejected?: IStockTransaction[];
}

export interface IGroupedByCreatedAt<T> {
  T: IStockTransaction[];
}
