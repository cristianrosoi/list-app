import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { IStockTransaction } from 'src/app/models/stock-transaction.model';
import { Observable, BehaviorSubject } from 'rxjs';
import { environment } from './../../../environments/environment.prod';
import { first, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class StockTransactionsService {

  public stockTransactions: IStockTransaction[];

  constructor(private http: HttpClient) { }

  public getStockTransactions(): Observable<IStockTransaction[]> {
    const url = environment.serviceURL.stockTransactionsURL;
    return this.http.get<IStockTransaction[]>(url);
  }
}
