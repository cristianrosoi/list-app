import { environment } from './../../../environments/environment';
import { IStockTransaction } from './../../models/stock-transaction.model';
import { TestBed, async } from '@angular/core/testing';

import { StockTransactionsService } from './stock-transactions.service';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

describe('StockTransactionsService', () => {
  let service: StockTransactionsService;
  let httpMock: HttpTestingController;

  beforeEach(() => TestBed.configureTestingModule({
    imports: [
      HttpClientTestingModule
    ],
    providers: [
      StockTransactionsService
    ]
  }));

  beforeEach(() => {
    service = TestBed.get(StockTransactionsService);
    httpMock = TestBed.get(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should get data', async(() => {
    service.getStockTransactions()
      .subscribe(
        (results: IStockTransaction[]) => {
          expect(results[0].name).toBeDefined();
          expect(results[0].description).toBeDefined();
          expect(results[0].createdAt).toBeDefined();
          expect(results[0].status).toBeDefined();
        }
      );

    const url = environment.serviceURL.stockTransactionsURL;
    const req = httpMock.expectOne(url);

    expect(req.request.method).toBe('GET');
  }));
});
