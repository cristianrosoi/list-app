import { FormatDatePipe } from './format-date.pipe';

describe('FormatDatePipe', () => {
  it('create an instance', () => {
    const pipe = new FormatDatePipe();
    expect(pipe).toBeTruthy();
  });

  it('should format the date: dd.mm.yyyy', () => {
    const inputDate = new Date('2019-09-10');
    const pipe = new FormatDatePipe();

    const date = pipe.transform(inputDate);

    expect(date).toEqual('10.09.2019');
  });
});
