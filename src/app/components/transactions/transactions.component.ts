import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, BehaviorSubject } from 'rxjs';
import { IStockTransaction } from 'src/app/models/stock-transaction.model';
import { StockTransactionsService } from './../../shared/services/stock-transactions.service';
import { FormGroup, FormBuilder } from '@angular/forms';
import { EListGroup, IGroupedByStatus, IGroupedByCreatedAt } from 'src/app/models/list-group.model';
import { groupBy } from 'src/app/utils/group-by.util';
import { tap, map, takeUntil } from 'rxjs/operators';

@Component({
  selector: 'app-transactions',
  templateUrl: './transactions.component.html',
  styleUrls: ['./transactions.component.scss']
})
export class TransactionsComponent implements OnInit, OnDestroy {

  public stockTransactions$: BehaviorSubject<IStockTransaction[]> = new BehaviorSubject<IStockTransaction[]>([]);
  public stockTransactions: IStockTransaction[];
  public groupTransactionsForm: FormGroup;

  public groupedByStatus: IGroupedByStatus;
  public groupedByStatusKeys: Array<string>;
  public groupedByCreatedAt: IGroupedByCreatedAt<string>;
  public groupedByCreatedAtKeys: Array<string>;

  public isSorted = true;
  private unsubscribe$: Subject<void> = new Subject<void>();


  constructor(private stockTransactionService: StockTransactionsService, private fb: FormBuilder) { }

  ngOnInit() {
    this.groupTransactionsForm = this.fb.group({
      transactionGroup: [''],
      filterByDescription: ['']
    });

    this.getStockTransactions();
    this.updateTransactions();
  }

  public getStockTransactions(): void {
    this.stockTransactionService.getStockTransactions()
      .pipe(
        takeUntil(this.unsubscribe$),
        tap((transactions: IStockTransaction[]) => transactions.sort((a, b) => a.name - b.name)),
        map((transactions: IStockTransaction[]) => {
          for (const item of transactions) {
            item.createdAt = new Date(item.createdAt);
          }
          return transactions;
        })
      )
      .subscribe(
        (transactions: IStockTransaction[]) => {
          // update list state
          this.stockTransactions = transactions;
          this.stockTransactionService.stockTransactions = transactions;
        },
        error => console.error(`Caught Erorr: Can't get stock transactions`, error),
        () => {}
      );
  }

  public updateTransactions(): void {
    this.stockTransactions$
    .pipe(takeUntil(this.unsubscribe$))
    .subscribe(
      (transactions: IStockTransaction[]) => this.stockTransactions = transactions,
      error => console.error(`Caught Error: Can't update stockTransactions`, error),
      () => {}
    );
  }

  public groupList(type: string) {

    this.resetGrouping();
    switch (type) {
      case type = EListGroup.status:
        this.groupedByStatus = groupBy(this.stockTransactions, EListGroup.status);
        this.groupedByStatusKeys = Object.keys(this.groupedByStatus);
        break;

      case type = EListGroup.createdAt:
        this.groupedByCreatedAt = groupBy(this.stockTransactions, EListGroup.createdAt);
        this.groupedByCreatedAtKeys = Object.keys(this.groupedByCreatedAt);
        break;
    }
  }

  public sortGroupedByStatus(key: string): IStockTransaction[] {
    let sortedList: IStockTransaction[];

    if (!this.isSorted) {
      sortedList = this.groupedByStatus[key].sort((a: IStockTransaction, b: IStockTransaction) => a.name - b.name);
      this.isSorted = true;
    } else {
      sortedList = this.groupedByStatus[key].sort((a: IStockTransaction, b: IStockTransaction) => b.name - a.name);
      this.isSorted = false;
    }

    return sortedList;
  }

  public sortGroupedByCreatedAt(key: string): void {
    let sortedList: IStockTransaction[];

    if (!this.isSorted) {
      sortedList = this.groupedByCreatedAt[key].sort((a: IStockTransaction, b: IStockTransaction) => a.name - b.name);
      this.isSorted = true;
    } else {
      sortedList = this.groupedByCreatedAt[key].sort((a: IStockTransaction, b: IStockTransaction) => b.name - a.name);
      this.isSorted = false;
    }
  }

  public resetGrouping(resetForm?: boolean): void {
    this.groupedByStatus = undefined;
    this.groupedByCreatedAt = undefined;
    this.groupedByStatusKeys = undefined;
    this.groupedByCreatedAtKeys = undefined;

    if (resetForm) {
      this.groupTransactionsForm.reset();
      this.unsubscribe$.next();
      this.unsubscribe$.complete();
      this.getStockTransactions();
      this.updateTransactions();
    }
  }

  public filterByDescription(): void {
    const value = this.groupTransactionsForm.controls.filterByDescription.value.toLowerCase();
    let list: IStockTransaction[];

    if (!value) {
      this.stockTransactions = this.stockTransactionService.stockTransactions;
    } else {
      list = this.stockTransactions.filter((transaction: IStockTransaction) => (transaction.description.toLowerCase()).includes(value));

      this.stockTransactions$.next(list);
    }

  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

}
