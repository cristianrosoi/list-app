import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockTransactionsService } from 'src/app/shared/services/stock-transactions.service';
import { TransactionsComponent } from './transactions.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormatDatePipe } from 'src/app/shared/pipes/format-date.pipe';
import { EListGroup } from 'src/app/models/list-group.model';

describe('TransactionsComponent', () => {
  let component: TransactionsComponent;
  let fixture: ComponentFixture<TransactionsComponent>;
  let stockTransactionsService: StockTransactionsService;

  const mockTransactions = [
    {
      name: 10568,
      description: 'Genetic Technologies Ltd',
      createdAt: new Date('2019-01-28'),
      status: 'completed'
    },
    {
      name: 10569,
      description: 'Sunworks, Inc.',
      createdAt: new Date('2019-01-28'),
      status: 'completed'
    },
    {
      name: 10570,
      description: 'U.S. Energy Corp.',
      createdAt: new Date('2019-01-28'),
      status: 'pending'
    },
    {
      name: 10571,
      description: 'Grupo Aeroportuario Del Pacifico, S.A. de C.V.',
      createdAt: new Date('2019-03-17'),
      status: 'pending'
    },
    {
      name: 10572,
      description: 'Landmark Infrastructure Partners LP',
      createdAt: new Date('2019-03-17'),
      status: 'completed'
    }
  ];

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        TransactionsComponent,
        FormatDatePipe
      ],
      imports: [
        FormsModule,
        ReactiveFormsModule,
        HttpClientTestingModule
      ],
      providers: [
        StockTransactionsService
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    stockTransactionsService = TestBed.get(StockTransactionsService);
    fixture = TestBed.createComponent(TransactionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should group the list by Status', () => {
    const type = EListGroup.status;
    component.stockTransactions = mockTransactions;

    component.groupList(type);

    expect(component.groupedByStatusKeys.length).toEqual(2);
    expect(component.groupedByStatus.pending.length).toEqual(2);
    expect(component.groupedByStatus.completed.length).toEqual(3);
  });

  it('should group the list by CreatedAt', () => {
    const type = EListGroup.createdAt;
    component.stockTransactions = mockTransactions;

    component.groupList(type);

    expect(component.groupedByCreatedAtKeys.length).toEqual(2);
  });

  it('should sort the groupByStatus', () => {
    // tslint:disable-next-line: no-string-literal
    component['isSorted'] = true;
    component.groupedByStatus = {
      pending: [{
        name: 10570,
        description: 'U.S. Energy Corp.',
        createdAt: new Date('2019-01-28'),
        status: 'pending'
      },
      {
        name: 10571,
        description: 'Grupo Aeroportuario Del Pacifico, S.A. de C.V.',
        createdAt: new Date('2019-03-17'),
        status: 'pending'
      }]
    };

    component.sortGroupedByStatus('pending');

    // tslint:disable-next-line: no-string-literal
    expect(component['isSorted']).toEqual(false);
    expect(component.groupedByStatus).toEqual({
      pending: [
        {
          name: 10571,
          description: 'Grupo Aeroportuario Del Pacifico, S.A. de C.V.',
          createdAt: new Date('2019-03-17'),
          status: 'pending'
        },
        {
          name: 10570,
          description: 'U.S. Energy Corp.',
          createdAt: new Date('2019-01-28'),
          status: 'pending'
        }
      ]
    });
  });

  it('should reset the grouping', () => {
    component.resetGrouping();

    expect(component.groupedByStatus).toBeUndefined();
    expect(component.groupedByStatusKeys).toBeUndefined();
    expect(component.groupedByCreatedAt).toBeUndefined();
    expect(component.groupedByCreatedAtKeys).toBeUndefined();
  });
});
