export const environment = {
  production: true,
  serviceURL: {
    stockTransactionsURL: 'assets/mock/list-data.json'
  }
};
